//https://www.luogu.com.cn/problem/P4779 只能得32分 
//https://www.luogu.com.cn/problem/P3371 可以通过，但需稍加改动 
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<ll,ll> pll;
vector<pll> g[100005];
ll n,m,s;
ll dis[100005];
bool vis[100005];	//是否在队列中 

queue<ll> q;
void spfa(ll n)
{
	memset(dis,0x3f,sizeof(dis));
    dis[s]=0;
    q.push(s);
    vis[s]=s;
    
    while (!q.empty()) {
        ll now = q.front(); q.pop();
        vis[now]=0;
        for(auto to:g[now])	//to是一个pair<ll,ll> 
        {
        	ll w=to.second,v=to.first;
			if(dis[now]+w<dis[v])
            {
                dis[v]=dis[now]+w;
                if(vis[v]==0)
                {
                    q.push(v);
                    vis[v]=1;
                }
            }
        }
    }
}

int main()
{
    scanf("%lld%lld%lld",&n,&m,&s);
    ll u,v,w;
    for( int i = 1 ; i <= m; i++ )
    {
        scanf("%lld%lld%lld",&u,&v,&w);
        g[u].push_back( {v,w} );
    }
    spfa(n);
    for(int i=1;i<=n;i++){
        printf("%lld%c",dis[i],(i==n)?'\n':' ');	//未处理不可达得情况 
    }
   return 0;
}
