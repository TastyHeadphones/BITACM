//本代码为VJ上A题“拓扑排序·一”示例代码 
#include<bits/stdc++.h>
using namespace std;
int T,n,m,u,v;
int deg[100005];	//deg[i]表示i的入度 
int vis[100005];	//思考一下，没有vis数组会出现什么问题？ 
vector<int> g[100005];
queue<int> q;

bool toposort(){
	int num=0;	//num是已经排好序的点的个数 
	while(!q.empty()){
		int now=q.front();q.pop();
		num++; 
		for(auto to:g[now]){	//别说这行看不懂奥 
			if(vis[to]){
				continue;
			} 
			else{
				deg[to]--;
				if(!deg[to]){
					vis[to]=1;
					q.push(to);
				}
			}
		}
	}
	if(num==n){
		return true;
	}
	else{
		return false;
	}
}

int main(){
	cin>>T;
	while(T--){
		memset(deg,0,sizeof(deg));
		memset(vis,0,sizeof(vis));
		for(int i=1;i<=100000;i++){
			g[i].clear();
		}
		while(!q.empty()){
			q.pop();
		}
		//以上是初始化 
		cin>>n>>m;
		for(int i=1;i<=m;i++){
			scanf("%d%d",&u,&v);
			g[u].push_back(v);
			deg[v]++;
		}
		for(int i=1;i<=n;i++){
			if(!deg[i]){
				vis[i]=1;
				q.push(i);
			}
		}
		if( toposort() ){
			puts("Correct");
		}
		else{
			puts("Wrong"); 
		}
	}
	return 0;
}

